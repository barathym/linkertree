

.. raw:: html

   <a rel="me" href="https://kolektiva.social/@raar"></a>
   <a rel="me" href="https://piaille.fr/@raar"></a>
   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://framapiaf.org/@goldman_bakounine"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>


.. https://framapiaf.org/web/tags/raar.rss
.. https://framapiaf.org/web/tags/pourim.rss
.. https://framapiaf.org/web/tags/golem.rss
.. https://framapiaf.org/web/tags/racisme.rss
.. https://framapiaf.org/web/tags/feminisme.rss

.. 🚧 👍 ❓
.. ✍🏼 ✍🏻✍🏿
.. ♀️✊ ⚖️ 📣
.. ✊🏻✊🏼✊🏽✊🏾✊🏿
.. 🤥 😍 ❤️
.. 🤪
.. ⚖️ 👨‍🎓
.. 🔥
.. 💧
.. ☠️
.. ☣️
.. 🪧
.. 🇺🇳
.. 🇫🇷
.. 🇧🇪
.. 🇺🇦
.. 🇬🇧
.. 🇨🇭
.. 🇩🇪 🎯
.. 🌍 ♀️✊🏽
.. 🇮🇷
.. 🎥 🎦
.. 🎇 🎉
.. un·e


|FluxWeb| `RSS <https://barathym.frama.io/linkertree/rss.xml>`_

.. _liens_barathym:

===================================================================================
|barathym| **Liens du café Barathym de Grenoble**
===================================================================================

::

    97 Galerie de l'Arlequin, Grenoble, France
    09 82 48 29 21
    caferesto@barathym.org



Sur le réseau commercial
============================

- https://fr-fr.facebook.com/barathym/


Barathym 2024
==============

- https://barathym.frama.io/barathym-2024/

Agenda sur Grenoble
=======================

- https://grenoble.frama.io/infos/agendas/agendas.html

Cité à Grenoble
================

- https://www.ici-grenoble.org/structure/bar-restaurant-le-barathym

